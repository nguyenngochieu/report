# -*- coding: utf-8 -*-
{
    'name': "Báo cáo (WeUp)",
    'summary': """Báo cáo""",
    'description': """Báo cáo""",
    'author': "ERP - WeUp",
    'website': "https://weupgroup.vn",
    'category': 'WeUp',
    'version': '1.0',
    'depends': [
        'base' ,'product', 'crm', 'contacts', 'sale_management', 'account'
    ],
    'data': [
        'security/ir.model.access.csv',
        # 'views/x_treatment_view.xml',
        'views/report_view.xml'
    ],
    'qweb': [],
    'installable': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: