# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError

class AccountMoveModel(models.Model):
    # Bảng hóa đơn
    _inherit = 'account.move'
    # Tạo các trường mới
    x_disease_id = fields.Many2one('x_disease', string='Đầu bệnh chính', store=True)
    x_disease_ids = fields.Many2many('x_disease', string="Đầu bệnh phụ")
    x_treatment_id = fields.Many2one('x_treatment', string='Liệu trình', store=True)
    
class SaleOrderModel(models.Model):
    # Bảng đơn hàng 
    _inherit = 'sale.order'
    # Tạo các trường mới
    x_disease_id = fields.Many2one('x_disease', string='Đầu bệnh chính', store=True)
    x_disease_ids = fields.Many2many('x_disease', string='Đầu bệnh phụ')
    x_treatment_id = fields.Many2one('x_treatment', string='Liệu trình', store=True)
    # Bắn các thông tin từ đơn hàng lên hóa đơn khi từ đơn hàng ấn tạo lên hóa đơn ở đây là đầu bệnh và liệu trình khi tạo hóa đơn sẽ bắn kèm các trường này sang bên hóa đơn
    def _prepare_invoice(self):
        res = super(SaleOrderModel, self)._prepare_invoice()
        res['x_disease_id']=self.x_disease_id
        res['x_disease_ids']=self.x_disease_ids
        res['x_treatment_id']=self.x_treatment_id
        return res


class TreatmentModel(models.Model):
    # Bảng liệu trình
    _name = 'x_treatment'
    _description = 'Liệu trình'
    name = fields.Char('Liệu trình', required=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id if self.env.company.id else None, string="Công ty")


class AccountInvoiceReportModel(models.Model):
    # Báo cáo trong module hóa đơn
    _inherit = 'account.invoice.report'
    x_disease_id = fields.Many2one('x_disease', string='Đầu bệnh chính', store=True)
    x_treatment_id = fields.Many2one('x_treatment', string='Liệu trình', store=True)
    
    _depends = {
        'account.move': [
            'name', 'state', 'move_type', 'partner_id', 'invoice_user_id', 'fiscal_position_id',
            'invoice_date', 'invoice_date_due', 'invoice_payment_term_id', 'partner_bank_id',
        ],
        'account.move.line': [
            'quantity', 'price_subtotal', 'amount_residual', 'balance', 'amount_currency',
            'move_id', 'product_id', 'product_uom_id', 'account_id', 'analytic_account_id',
            'journal_id', 'company_id', 'currency_id', 'partner_id',
        ],
        'product.product': ['product_tmpl_id'],
        'product.template': ['categ_id'],
        'uom.uom': ['category_id', 'factor', 'name', 'uom_type'],
        'res.currency.rate': ['currency_id', 'name'],
        'res.partner': ['country_id'],
    }

    @property
    def _table_query(self):
        return '%s %s %s' % (self._select(), self._from(), self._where())
    # Select các trường để có thể lọc ra ở phần báo cáo biểu đồ
    @api.model
    def _select(self):
        return '''
            SELECT
                line.id,
                line.move_id,
                line.product_id,
                line.account_id,
                line.analytic_account_id,
                line.journal_id,
                line.company_id,
                line.company_currency_id,
                line.partner_id AS commercial_partner_id,
                move.state,
                move.move_type,
                move.partner_id,
                move.x_disease_id,
                move.x_treatment_id,
                move.invoice_user_id,
                move.fiscal_position_id,
                move.payment_state,
                move.invoice_date,
                move.invoice_date_due,
                uom_template.id                                             AS product_uom_id,
                template.categ_id                                           AS product_categ_id,
                line.quantity / NULLIF(COALESCE(uom_line.factor, 1) / COALESCE(uom_template.factor, 1), 0.0) * (CASE WHEN move.move_type IN ('in_invoice','out_refund','in_receipt') THEN -1 ELSE 1 END)
                                                                            AS quantity,
                -line.balance * currency_table.rate                         AS price_subtotal,
                -COALESCE(line.balance
                   / NULLIF(line.quantity, 0.0)
                   / NULLIF(COALESCE(uom_line.factor, 1), 0.0)
                   / NULLIF(COALESCE(uom_template.factor, 1), 0.0),
                   0.0) * currency_table.rate
                                                                            AS price_average,
                COALESCE(partner.country_id, commercial_partner.country_id) AS country_id
        '''

    @api.model
    def _from(self):
        return '''
            FROM account_move_line line
                LEFT JOIN res_partner partner ON partner.id = line.partner_id
                LEFT JOIN product_product product ON product.id = line.product_id
                LEFT JOIN account_account account ON account.id = line.account_id
                LEFT JOIN account_account_type user_type ON user_type.id = account.user_type_id
                LEFT JOIN product_template template ON template.id = product.product_tmpl_id
                LEFT JOIN uom_uom uom_line ON uom_line.id = line.product_uom_id
                LEFT JOIN uom_uom uom_template ON uom_template.id = template.uom_id
                INNER JOIN account_move move ON move.id = line.move_id
                LEFT JOIN res_partner commercial_partner ON commercial_partner.id = move.commercial_partner_id
                JOIN {currency_table} ON currency_table.company_id = line.company_id
        '''.format(
            currency_table=self.env['res.currency']._get_query_currency_table({'multi_company': True, 'date': {'date_to': fields.Date.today()}}),
        )

    @api.model
    def _where(self):
        return '''
            WHERE move.move_type IN ('out_invoice', 'out_refund', 'in_invoice', 'in_refund', 'out_receipt', 'in_receipt')
                AND line.account_id IS NOT NULL
                AND NOT line.exclude_from_invoice_tab
        '''






  
